<?php
/**
 * Created by PhpStorm.
 * User: emreg
 * Date: 19.01.2018
 * Time: 11:31
 */
require_once ("ICardReaderAdapter.php");
require_once ("XPosReader.php");

class XPosReaderAdapter implements ICardReaderAdapter
{
    private $posReader;
    function __construct()
    {
        $this->posReader= new XPosReader();
    }

    public function Reader()
   {
       // TODO: Implement Reader() method.

       $this->posReader->PosReaderX();
   }
}