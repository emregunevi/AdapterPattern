<?php
/**
 * Created by PhpStorm.
 * User: emreg
 * Date: 19.01.2018
 * Time: 11:49
 */

require_once ("ICardReaderAdapter.php");
require_once ("YPosReader.php");

class YPosReaderAdapter implements ICardReaderAdapter
{
    private $yreader;
    function __construct()
    {
        $this->yreader = new YPosReader();
    }

    public function Reader()
    {
        // TODO: Implement Reader() method.

         $this->yreader->PosReaderY();
    }
}